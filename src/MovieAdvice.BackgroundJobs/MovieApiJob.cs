using Hangfire;

namespace MovieAdvice.BackgroundJobs
{
    public static class MovieApiJob
    {
        public static void GetMoviesFromApi()
        {
            RecurringJob.RemoveIfExists(nameof(MovieDbService));
            RecurringJob.AddOrUpdate<MovieDbService>(nameof(MovieDbService), job => job.Process(), Cron.Hourly);
        }
    }
}