using System;
using System.Threading.Tasks;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;

namespace MovieAdvice.BackgroundJobs
{
    public class MovieDbService
    {
        private readonly IMovieUpdateService movieService;

        public MovieDbService(IMovieUpdateService movieService)
        {
            this.movieService = movieService;
        }
        
        public async Task Process()
        {
            var isAdded = await movieService.UpdateMovies();
            if (isAdded)
            {
                Console.WriteLine(Messages.DatabaseUpdated);   
            }
        }
    }
}