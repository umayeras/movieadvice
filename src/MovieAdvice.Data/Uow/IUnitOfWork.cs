using System;
using MovieAdvice.Data.Repositories;

namespace MovieAdvice.Data.Uow
{
    public interface IUnitOfWork : IDisposable
    {
        int Save();
        
        IBaseRepository<TEntity> GetRepository<TEntity>() where TEntity : class;
    }
}