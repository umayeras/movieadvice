using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MovieAdvice.Core.Extensions;

namespace MovieAdvice.Data.Repositories
{
    public class BaseRepository<T> : IBaseRepository<T>
        where T : class
    {
        #region ctor

        private readonly DbContext context;
        private readonly DbSet<T> dbSet;
        private readonly ILogger<BaseRepository<T>> logger;

        public BaseRepository(DbContext context, ILogger<BaseRepository<T>> logger)
        {
            this.context = context ?? throw new ArgumentException(nameof(context));
            this.logger = logger;
            
            dbSet = context.Set<T>();
        }

        #endregion

        public IQueryable<T> GetAll()
        {
            return dbSet;
        }
        
        public async Task<IQueryable<T>> GetAllAsync(Expression<Func<T, bool>> filter = null)
        {
            var list = filter == null
                ? await dbSet.AsNoTracking().ToListAsync()
                : await dbSet.Where(filter).AsNoTracking().ToListAsync();

            return list.AsQueryable();
        }

        public async Task<T> GetAsync(Expression<Func<T, bool>> filter)
        {
            return await dbSet.SingleOrDefaultAsync(filter);
        }

        public async Task<bool> AddAsync(T entity)
        {
            try
            {
                await dbSet.AddAsync(entity);
                return true;
            }
            catch (Exception ex)
            {
                var message = GetLogMessage(entity, nameof(AddAsync), ex.Message);
                logger.LogError(message);
                return false;
            }
        }

        public bool Add(T entity)
        {
            try
            {
                dbSet.Add(entity);
                return true;
            }
            catch (Exception ex)
            {
                var message = GetLogMessage(entity, nameof(Add), ex.Message);
                logger.LogError(message);
                
                return false;
            }
        }

        public bool Update(T entity)
        {
            try
            {
                dbSet.Attach(entity); 
                context.Entry(entity).State = EntityState.Modified;
                
                return true;
            }
            catch (Exception ex)
            {
                var message = GetLogMessage(entity, nameof(Update), ex.Message);
                logger.LogError(message);
                return false;
            }
        }

        public bool Delete(T entity)
        {
            try
            {
                dbSet.Attach(entity); 
                context.Entry(entity).State = EntityState.Modified;
                
                return true;
            }
            catch (Exception ex)
            {
                var message = GetLogMessage(entity, nameof(Delete), ex.Message);
                logger.LogError(message);

                return false;
            }
        }

        public bool DeleteItem(T entity)
        {
            try
            {
                dbSet.Remove(entity);
                return true;
            }
            catch (Exception ex)
            {
                var message = GetLogMessage(entity, nameof(DeleteItem), ex.Message);
                logger.LogError(message);

                return false;
            }
        }

        private static string GetLogMessage(T entity, string methodName, string message)
        {
            return entity.GetType().CreateLogMessage(methodName, message);
        }
    }
}