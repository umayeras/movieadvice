using Microsoft.EntityFrameworkCore;
using MovieAdvice.Model.Entities;

namespace MovieAdvice.Data
{
    public class MovieAdviceDbContext : DbContext
    {
        public MovieAdviceDbContext(DbContextOptions<MovieAdviceDbContext> options)
            : base(options)
        {
        }
        
        public DbSet<Movie> Movies { get; set; }
        public DbSet<MovieComment> MovieComments { get; set; }
        public DbSet<MovieScore> MovieScores { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Genre> Genres { get; set; }
    }
}