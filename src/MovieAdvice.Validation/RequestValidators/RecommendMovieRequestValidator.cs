﻿using FluentValidation;
using MovieAdvice.Model.Requests;

namespace MovieAdvice.Validation.RequestValidators
{
    public class RecommendMovieRequestValidator : AbstractValidator<RecommendMovieRequest>
    {
        public RecommendMovieRequestValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Lütfen email adresi alanını boş bırakmayınız!");

            RuleFor(x => x.MovieId)
                .NotEmpty()
                .WithMessage("Lütfen film alanını boş bırakmayınız!");
        }
    }
}