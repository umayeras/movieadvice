﻿using FluentValidation;
using MovieAdvice.Model.Requests;

namespace MovieAdvice.Validation.RequestValidators
{
    public class AddCommentRequestValidator : AbstractValidator<AddCommentRequest>
    {
        public AddCommentRequestValidator()
        {
            RuleFor(x => x.MovieId)
                .NotEmpty()
                .WithMessage("Lütfen MovieId alanını boş bırakmayınız!")
                .GreaterThan(0)
                .WithMessage("MovieId değeri 0' dan büyük olmalıdır");
            
            RuleFor(x => x.Comment)
                .NotEmpty()
                .WithMessage("Lütfen Yorum alanını boş bırakmayınız!");
        }
    }
}