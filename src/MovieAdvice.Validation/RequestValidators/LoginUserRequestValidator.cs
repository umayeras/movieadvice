﻿using FluentValidation;
using MovieAdvice.Model.Requests;

namespace MovieAdvice.Validation.RequestValidators
{
    public class LoginUserRequestValidator : AbstractValidator<LoginUserRequest>
    {
        public LoginUserRequestValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Lütfen email adresi alanını boş bırakmayınız!");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Lütfen şifre alanını boş bırakmayınız!");
        }
    }
}