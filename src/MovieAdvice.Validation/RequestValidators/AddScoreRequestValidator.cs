﻿using FluentValidation;
using MovieAdvice.Model.Requests;

namespace MovieAdvice.Validation.RequestValidators
{
    public class AddScoreRequestValidator : AbstractValidator<AddScoreRequest>
    {
        public AddScoreRequestValidator()
        {
            RuleFor(x => x.MovieId)
                .NotEmpty()
                .WithMessage("Lütfen MovieId alanını boş bırakmayınız!")
                .GreaterThan(0)
                .WithMessage("MovieId 0' dan büyük olmalıdır.");

            RuleFor(x => x.Score)
                .NotEmpty()
                .WithMessage("Lütfen Score alanını boş bırakmayınız!")
                .InclusiveBetween(1,10)
                .WithMessage("Score değeri 1 ile 10 aralığında olmalıdır");
        }
    }
}