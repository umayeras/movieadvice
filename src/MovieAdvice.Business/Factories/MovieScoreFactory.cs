using MovieAdvice.Business.Abstract.Factories;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;

namespace MovieAdvice.Business.Factories
{
    public class MovieScoreFactory : IMovieScoreFactory
    {
        public MovieScore CreateMovieScore(AddScoreRequest request, int userId)
        {
            return new MovieScore
            {
                MovieId = request.MovieId,
                CreatedBy = userId,
                Score = request.Score
            };
        }
    }
}