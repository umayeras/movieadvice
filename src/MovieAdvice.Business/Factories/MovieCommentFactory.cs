using MovieAdvice.Business.Abstract.Factories;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;

namespace MovieAdvice.Business.Factories
{
    public class MovieCommentFactory : IMovieCommentFactory
    {
        public MovieComment CreateMovieComment(AddCommentRequest request, int userId)
        {
            return new MovieComment
            {
                MovieId = request.MovieId,
                CreatedBy = userId,
                Comment = request.Comment
            };
        }
    }
}