using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MovieAdvice.Business.Abstract.Factories;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;

namespace MovieAdvice.Business.Services
{
    public class MovieCommentService : BaseService<MovieCommentService>, IMovieCommentService
    {
        private readonly IBaseRepository<MovieComment> repository;
        private readonly IMovieCommentFactory factory;

        public MovieCommentService(
            IUnitOfWork unitOfWork,
            ILogger<MovieCommentService> logger, 
            IMovieCommentFactory factory) : base(unitOfWork, logger)
        {
            this.factory = factory;
            repository = unitOfWork.GetRepository<MovieComment>();
        }

        public async Task<ServiceResult> Add(AddCommentRequest request, int userId)
        {
            var comment = factory.CreateMovieComment(request, userId);

            try
            {
                await repository.AddAsync(comment);
                UnitOfWork.Save();

                return ServiceResult.Success(Messages.MovieCommentAdded);
            }
            catch (Exception ex)
            {
                Logger.LogError($"MovieCommentService - {nameof(Add)}", ex.Message);
                return ServiceResult.Error(Messages.MovieCommentIsNotAdded);
            }
        }

        public async Task<IEnumerable<MovieComment>> GetUserComments(int userId, int movieId)
        {
            return await repository.GetAllAsync(x => x.MovieId == movieId && x.CreatedBy == userId);
        }
    }
}