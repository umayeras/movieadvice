using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.ExternalApi;
using Newtonsoft.Json;

namespace MovieAdvice.Business.Services
{
    public class MovieGenreService : BaseService<MovieGenreService>, IMovieGenreService
    {
        private readonly IBaseRepository<Genre> repository;
        private IConfiguration Configuration { get; }

        public MovieGenreService(
            IUnitOfWork unitOfWork,
            ILogger<MovieGenreService> logger,
            IConfiguration configuration) : base(unitOfWork, logger)
        {
            repository = UnitOfWork.GetRepository<Genre>();
            Configuration = configuration;
        }

        public async Task Add()
        {
            var settings = Configuration.GetSection(SectionNames.MovieDbSettings).Get<MovieDbSettings>();
            var baseAddress = new Uri(settings.BaseAddress);
            var requestUri = $"{settings.GenresPath}?api_key={settings.ApiKey}";
            
            var responseData = await GetDataAsync(baseAddress, requestUri);
            var model = JsonConvert.DeserializeObject<GenresResult>(responseData);
            
            foreach (var item in model.Genres)
            {
                var genre = CreateGenre(item);
                await repository.AddAsync(genre);
            }
            
            UnitOfWork.Save();
        }
        
        public async Task<IEnumerable<Genre>> GetAll()
        {
            return await repository.GetAllAsync();
        }
        
        private static Genre CreateGenre(GenreResult item)
        {
            return new Genre
            {
                Id = item.Id,
                Name = item.Name
            };
        }

    }
}