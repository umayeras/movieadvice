using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Core.Security.Abstract;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;

namespace MovieAdvice.Business.Services
{
    public class AccountService : BaseService<AccountService>, IAccountService
    {
        private readonly IBaseRepository<User> repository;
        private readonly ITokenHelper tokenHelper;

        public AccountService(
            IUnitOfWork unitOfWork,
            ILogger<AccountService> logger,
            ITokenHelper tokenHelper) : base(unitOfWork, logger)
        {
            repository = UnitOfWork.GetRepository<User>();
            this.tokenHelper = tokenHelper;
        }

        public async Task<ServiceDataResult> Authenticate(LoginUserRequest request)
        {
            var user = await repository.GetAsync(x => x.Email == request.Email);
            if (user == null)
            {
                Logger.LogError($"AccountService - {nameof(Authenticate)}", Messages.UserNotFound);

                return ServiceDataResult.Error(Messages.UserNotFound);
            }

            var token = tokenHelper.CreateToken(user);
            if (token == null)
            {
                Logger.LogError($"AccountService - {nameof(Authenticate)}", Messages.TokenNotCreated);

                return ServiceDataResult.Error(Messages.TokenNotCreated);
            }

            return ServiceDataResult.Success(token);
        }

        public async Task<ServiceDataResult> RegisterUser(RegisterUserRequest request)
        {
            var user = await repository.GetAsync(x => x.Email == request.Email);
            if (user != null)
            {
                return ServiceDataResult.Error(Messages.UserRegisteredBefore);
            }

            var newUser = new User
            {
                Email = request.Email,
                Password = request.Password
            };

            var isRegistered = await AddUser(newUser);

            if (!isRegistered)
            {
                Logger.LogError($"AccountService - {nameof(RegisterUser)}", Messages.GeneralError);

                return ServiceDataResult.Error(Messages.UserRegistrationError);
            }

            var token = tokenHelper.CreateToken(newUser);
            if (token == null)
            {
                Logger.LogError($"AccountService - {nameof(RegisterUser)}", Messages.TokenNotCreated);

                return ServiceDataResult.Error(Messages.TokenNotCreated);
            }

            return ServiceDataResult.Success(token);
        }

        private async Task<bool> AddUser(User newUser)
        {
            var isRegistered = await repository.AddAsync(newUser);
            UnitOfWork.Save();
            
            return isRegistered;
        }

        public async Task<User> GetUser(int userId)
        {
            return await repository.GetAsync(x => x.Id == userId);
        }
    }
}