using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Core.Mailing;
using MovieAdvice.Core.Mailing.Abstract;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Results;

namespace MovieAdvice.Business.Services
{
    public class MovieRecommendService : IMovieRecommendService
    {
        private IConfiguration Configuration { get; }
        private readonly ILogger<MovieRecommendService> logger;
        private readonly IEmailService emailService;
        private readonly IAccountService accountService;

        public MovieRecommendService(
            IEmailService emailService,
            IAccountService accountService, 
            IConfiguration configuration, 
            ILogger<MovieRecommendService> logger)
        {
            Configuration = configuration;
            this.logger = logger;
            this.emailService = emailService;
            this.accountService = accountService;
        }
        
        public async Task<ServiceResult> RecommendMovie(Movie movie, int userId, string email)
        {
            var user = await accountService.GetUser(userId);
            var emailBody = $"{user.Email} size {movie.Title} isimli filmi tavsiye ediyor.";

            try
            {
                var settings = Configuration.GetSection(SectionNames.EmailSettings).Get<EmailSettings>();
                var emailRequest = new EmailRequest
                {
                    From = settings.EmailAddress,
                    To = email,
                    Subject = "Film Tavsiyesi",
                    Body = emailBody
                };

                await emailService.SendEmailAsync(emailRequest);

                return ServiceResult.Success(Messages.RecommendEmailSent);
            }
            catch (Exception ex)
            {
                logger.LogError($"MovieService - {nameof(RecommendMovie)}", ex.Message);

                return ServiceResult.Error(Messages.GeneralError);
            }
        }
    }
}