using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Helpers.Pagination;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Results;

namespace MovieAdvice.Business.Services
{
    public class MovieService : BaseService<MovieService>, IMovieService
    {
        private readonly IBaseRepository<Movie> repository;
        private readonly IUriService uriService;
        private readonly IMovieCommentService commentService;
        private readonly IMovieScoreService scoreService;

        public MovieService(
            IUnitOfWork unitOfWork,
            ILogger<MovieService> logger,
            IUriService uriService,
            IMovieCommentService commentService,
            IMovieScoreService scoreService) : base(unitOfWork, logger)
        {
            this.uriService = uriService;
            this.commentService = commentService;
            this.scoreService = scoreService;

            repository = UnitOfWork.GetRepository<Movie>();
        }

        public async Task<PagedResponse<List<Movie>>> GetAllByPagination(string route, PaginationFilter filter)
        {
            var pagedData = await repository.GetAll()
                .Skip((filter.PageNumber - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ToListAsync();

            var totalRecords = await repository.GetAll().CountAsync();

            return PaginationHelper.CreatePagedResponse(pagedData, filter, totalRecords, uriService, route);
        }

        public async Task<MovieDetailResult> GetByDetail(int id, int userId)
        {
            var movie = await GetById(id);
            if (movie == null)
            {
                return new MovieDetailResult();
            }

            var comments = await commentService.GetUserComments(userId, id);
            var userScore = await scoreService.GetUserScore(userId, id);
            var averageScore = scoreService.GetAverageScore(id);

            return new MovieDetailResult
            {
                Movie = movie,
                Comments = comments,
                AverageScore = averageScore,
                UserScore = userScore
            };
        }

        public async Task<Movie> GetById(int id)
        {
            return await repository.GetAsync(x => x.Id == id);
        }
    }
}