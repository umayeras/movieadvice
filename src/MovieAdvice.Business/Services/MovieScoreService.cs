using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MovieAdvice.Business.Abstract.Factories;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;

namespace MovieAdvice.Business.Services
{
    public class MovieScoreService : BaseService<MovieScoreService>, IMovieScoreService
    {
        private readonly IBaseRepository<MovieScore> repository;
        private readonly IMovieScoreFactory factory;

        public MovieScoreService(
            IUnitOfWork unitOfWork,
            ILogger<MovieScoreService> logger,
            IMovieScoreFactory factory) : base(unitOfWork, logger)
        {
            this.factory = factory;
            repository = unitOfWork.GetRepository<MovieScore>();
        }

        public async Task<ServiceResult> Add(AddScoreRequest request, int userId)
        {
            var userScore = await GetUserScore(request, userId);
            if (userScore != null)
            {
                return ServiceResult.Error(Messages.MovieIsRatedBefore);
            }

            var movieScore = factory.CreateMovieScore(request, userId);

            try
            {
                await repository.AddAsync(movieScore);
                UnitOfWork.Save();

                return ServiceResult.Success(Messages.MovieScoreAdded);
            }
            catch (Exception ex)
            {
                Logger.LogError($"MovieScoreService - {nameof(Add)}", ex.Message);
                return ServiceResult.Error(Messages.MovieScoreIsNotAdded);
            }
        }

        private async Task<MovieScore> GetUserScore(AddScoreRequest request, int userId)
        {
            return await repository.GetAsync(x => x.MovieId == request.MovieId && x.CreatedBy == userId);
        }

        public async Task<int> GetUserScore(int userId, int movieId)
        {
            var userScore = await repository.GetAsync(x => x.MovieId == movieId && x.CreatedBy == userId);

            return userScore?.Score ?? 0;
        }

        public double GetAverageScore(int movieId)
        {
            return repository.GetAll().Where(x => x.MovieId == movieId).Average(x => x.Score);
        }
    }
}