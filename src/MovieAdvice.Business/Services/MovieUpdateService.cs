using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.ExternalApi;
using Newtonsoft.Json;

namespace MovieAdvice.Business.Services
{
    public class MovieUpdateService : BaseService<MovieUpdateService>, IMovieUpdateService
    {
        private readonly IBaseRepository<Movie> repository;
        private IConfiguration Configuration { get; }
        private readonly IMovieGenreService genreService;

        public MovieUpdateService(
            IUnitOfWork unitOfWork,
            ILogger<MovieUpdateService> logger,
            IConfiguration configuration,
            IMovieGenreService genreService) : base(unitOfWork, logger)
        {
            repository = UnitOfWork.GetRepository<Movie>();
            Configuration = configuration;
            this.genreService = genreService;
        }

        public async Task<bool> UpdateMovies()
        {
            try
            {
                var externalMovieList = await GetMoviesFromApi();
                var movies = await repository.GetAllAsync();
                var newMovies = externalMovieList.Where(x => !movies.Any(y => x.Title == y.Title));

                foreach (var movie in newMovies)
                {
                    await repository.AddAsync(movie);
                }

                UnitOfWork.Save();
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError($"MovieUpdateService - {nameof(UpdateMovies)}", ex.Message);
                return false;
            }
        }

        private async Task<ICollection<Movie>> GetMoviesFromApi()
        {
            var settings = Configuration.GetSection(SectionNames.MovieDbSettings).Get<MovieDbSettings>();
            var baseAddress = new Uri(settings.BaseAddress);
            
            var movies = new List<Movie>();
            var genres = await genreService.GetAll();
            foreach (var genre in genres)
            {
                var requestUri = $"{settings.MoviesPath}?api_key={settings.ApiKey}&with_genres={genre.Id}";
                var responseData = await GetDataAsync(baseAddress, requestUri);
                var model = JsonConvert.DeserializeObject<RootObject>(responseData);

                movies.AddRange(model.results.Select(CreateMovie));
            }
            
            return movies;
        }

        private static Movie CreateMovie(MovieResult result)
        {
            return new Movie
            {
                Title = result.title,
                Poster = result.poster_path,
                Overview = result.overview
            };
        }
    }
}