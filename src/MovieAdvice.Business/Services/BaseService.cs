using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MovieAdvice.Data.Uow;

namespace MovieAdvice.Business.Services
{
    public class BaseService<TService>
    {
        protected readonly IUnitOfWork UnitOfWork;
        protected readonly ILogger<TService> Logger;

        protected BaseService(IUnitOfWork unitOfWork, ILogger<TService> logger)
        {
            UnitOfWork = unitOfWork;
            Logger = logger;
        }
        
        protected static async Task<string> GetDataAsync(Uri baseAddress, string requestUri)
        {
            using (var httpClient = new HttpClient {BaseAddress = baseAddress})
            {
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("accept", "application/json");

                using (var response = await httpClient.GetAsync(requestUri))
                {
                    return await response.Content.ReadAsStringAsync();
                }
            }
        }
    }
}