using System.Collections.Generic;
using System.Threading.Tasks;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;

namespace MovieAdvice.Business.Abstract.Services
{
    public interface IMovieCommentService
    {
        Task<ServiceResult> Add(AddCommentRequest request, int userId);
        Task<IEnumerable<MovieComment>> GetUserComments(int userId, int movieId);
    }
}