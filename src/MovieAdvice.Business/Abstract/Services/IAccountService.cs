using System.Threading.Tasks;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;

namespace MovieAdvice.Business.Abstract.Services
{
    public interface IAccountService
    {
        Task<ServiceDataResult> Authenticate(LoginUserRequest request);
        Task<ServiceDataResult> RegisterUser(RegisterUserRequest request);
        Task<User> GetUser(int userId);
    }
}