using System.Collections.Generic;
using System.Threading.Tasks;
using MovieAdvice.Model.Entities;

namespace MovieAdvice.Business.Abstract.Services
{
    public interface IMovieGenreService
    {
        Task Add();
        Task<IEnumerable<Genre>> GetAll();
    }
}