using System.Threading.Tasks;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;

namespace MovieAdvice.Business.Abstract.Services
{
    public interface IMovieScoreService
    {
        Task<ServiceResult> Add(AddScoreRequest request, int userId);
        Task<int> GetUserScore(int userId, int movieId);
        double GetAverageScore(int movieId);
    }
}