using System.Threading.Tasks;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Results;

namespace MovieAdvice.Business.Abstract.Services
{
    public interface IMovieRecommendService
    {
        Task<ServiceResult> RecommendMovie(Movie movie, int userId, string email);
    }
}