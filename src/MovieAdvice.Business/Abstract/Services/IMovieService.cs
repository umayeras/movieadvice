using System.Collections.Generic;
using System.Threading.Tasks;
using MovieAdvice.Core.Helpers.Pagination;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Results;

namespace MovieAdvice.Business.Abstract.Services
{
    public interface IMovieService
    {
        Task<PagedResponse<List<Movie>>> GetAllByPagination(string route, PaginationFilter filter);
        Task<MovieDetailResult> GetByDetail(int id, int userId);
        Task<Movie> GetById(int id);
    }
}