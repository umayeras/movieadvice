using System.Threading.Tasks;

namespace MovieAdvice.Business.Abstract.Services
{
    public interface IMovieUpdateService
    {
        Task<bool> UpdateMovies();
    }
}