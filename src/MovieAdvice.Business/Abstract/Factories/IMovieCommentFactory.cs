using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;

namespace MovieAdvice.Business.Abstract.Factories
{
    public interface IMovieCommentFactory
    {
        MovieComment CreateMovieComment(AddCommentRequest request, int userId);
    }
}