using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;

namespace MovieAdvice.Business.Abstract.Factories
{
    public interface IMovieScoreFactory
    {
        MovieScore CreateMovieScore(AddScoreRequest request, int userId);
    }
}