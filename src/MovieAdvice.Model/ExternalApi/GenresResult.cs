using System.Collections.Generic;

namespace MovieAdvice.Model.ExternalApi
{
    public class GenresResult
    {
        public ICollection<GenreResult> Genres { get; set; }
    }
}