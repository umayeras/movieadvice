namespace MovieAdvice.Model.ExternalApi
{
    public class GenreResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}