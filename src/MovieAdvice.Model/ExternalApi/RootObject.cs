using System.Collections.Generic;

namespace MovieAdvice.Model.ExternalApi
{
    public class RootObject
    {
        public int page { get; set; }
        public List<MovieResult> results { get; set; }
        public int total_pages { get; set; }
        public int total_results { get; set; }
    }
}