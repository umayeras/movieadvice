﻿﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MovieAdvice.Model.Entities.Base
{
    public abstract class Entity<T> : IEntity<T>
    {
        public virtual T Id { get; set; }
        
        [ScaffoldColumn(false)]
        public DateTime CreatedDate { get; set; }
        
        [ScaffoldColumn(false)]
        public int CreatedBy { get; set; }

        protected Entity()
        {
            CreatedDate = DateTime.Now.ToLocalTime();
        }
    }
}
