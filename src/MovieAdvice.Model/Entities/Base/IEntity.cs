﻿﻿using System;

namespace MovieAdvice.Model.Entities.Base
{
    public interface IEntity<T>
    {
        T Id { get; set; }
        DateTime CreatedDate { get; set; }
        int CreatedBy { get; set; }
    }
}
