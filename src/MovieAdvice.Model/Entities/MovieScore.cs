﻿﻿using MovieAdvice.Model.Entities.Base;

namespace MovieAdvice.Model.Entities
{
    public class MovieScore :Entity<int>
    {
        public int MovieId { get; set; }
        public int Score { get; set; }
    }
}