﻿﻿using MovieAdvice.Model.Entities.Base;

namespace MovieAdvice.Model.Entities
{
    public class MovieComment :Entity<int>
    {
        public int MovieId { get; set; }
        public string Comment { get; set; }
    }
}