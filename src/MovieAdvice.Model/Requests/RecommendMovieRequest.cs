namespace MovieAdvice.Model.Requests
{
    public class RecommendMovieRequest
    {
        public string Email { get; set; }
        public int MovieId { get; set; }
    }
}