namespace MovieAdvice.Model.Requests
{
    public class AddCommentRequest
    {
        public int MovieId { get; set; }
        public string Comment { get; set; }
    }
}