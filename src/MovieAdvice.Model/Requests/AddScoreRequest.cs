namespace MovieAdvice.Model.Requests
{
    public class AddScoreRequest
    {
        public int MovieId { get; set; }
        public int Score { get; set; }
    }
}