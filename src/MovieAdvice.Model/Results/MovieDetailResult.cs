using System.Collections.Generic;
using MovieAdvice.Model.Entities;

namespace MovieAdvice.Model.Results
{
    public class MovieDetailResult
    {
        public Movie Movie { get; set; }
        public IEnumerable<MovieComment> Comments { get; set; }
        
        public double AverageScore { get; set; }
        public int UserScore { get; set; }
    }
}