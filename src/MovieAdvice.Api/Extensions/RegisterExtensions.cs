﻿using System;
using System.Collections.Generic;
using FluentValidation.AspNetCore;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using MovieAdvice.Business.Factories;
using MovieAdvice.Business.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Core.Helpers.Pagination;
using MovieAdvice.Core.Mailing;
using MovieAdvice.Core.Mailing.Abstract;
using MovieAdvice.Core.Security;
using MovieAdvice.Core.Security.Abstract;
using MovieAdvice.Data;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Validation;
using MovieAdvice.Validation.Abstract;
using MovieAdvice.Validation.RequestValidators;
using NetCore.AutoRegisterDi;

namespace MovieAdvice.Api.Extensions
{
    internal static class RegisterExtensions
    {
        internal static void AddDatabaseContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString(AppSettings.DefaultConnection);
            services.AddDbContext<MovieAdviceDbContext>(options => { options.UseSqlServer(connectionString); });
        }

        internal static void AddDependencyResolvers(this IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.RegisterAssemblyPublicNonGenericClasses(typeof(MovieService).Assembly)
                .Where(x => x.Name.EndsWith(ClassSuffix.Service))
                .AsPublicImplementedInterfaces(ServiceLifetime.Scoped);

            services.RegisterAssemblyPublicNonGenericClasses(typeof(MovieCommentFactory).Assembly)
                .Where(x => x.Name.EndsWith(ClassSuffix.Factory))
                .AsPublicImplementedInterfaces(ServiceLifetime.Scoped);
            
            services.AddSingleton<IUriService>(o =>
            {
                var accessor = o.GetRequiredService<IHttpContextAccessor>();
                var request = accessor.HttpContext.Request;
                var uri = string.Concat(request.Scheme, "://", request.Host.ToUriComponent());

                return new UriService(uri);
            });

            services.AddScoped<ITokenHelper, JwtHelper>();
            services.AddScoped<IEmailService, EmailService>();
        }

        internal static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Movie Advice App",
                    Description = "A sample project for ASP.NET Core Web API",
                    Contact = new OpenApiContact
                    {
                        Name = "Umay ERAS",
                        Email = "umayeras@hotmail.com",
                        Url = new Uri("http://umayeras.com"),
                    }
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });
            });
        }

        internal static void AddVersioning(this IServiceCollection services)
        {
            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
                config.ApiVersionReader = new HeaderApiVersionReader("X-version");
            });
        }

        internal static void AddFluentValidation(this IServiceCollection services)
        {
            services.AddMvc().AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssemblyContaining<AddScoreRequestValidator>();
                fv.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
            });

            services.AddSingleton<IRequestValidator, RequestValidator>();
        }

        internal static void AddHangfireForBackgroundJobs(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.AddHangfire(config =>
            {
                var option = new SqlServerStorageOptions
                {
                    PrepareSchemaIfNecessary = true,
                    QueuePollInterval = TimeSpan.FromMinutes(5),
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                };

                config.UseSqlServerStorage(configuration.GetConnectionString(AppSettings.DefaultConnection), option)
                    .WithJobExpirationTimeout(TimeSpan.FromHours(6));
            });

            services.AddHangfireServer();
        }

        internal static void AddAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            var tokenOptions = configuration.GetSection(SectionNames.TokenOptions).Get<TokenOptions>();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = SecurityHelper.CreateSecurityKey(tokenOptions.SecurityKey)
                    };
                });
        }
    }
}