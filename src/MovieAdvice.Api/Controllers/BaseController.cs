using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MovieAdvice.Validation.Abstract;

namespace MovieAdvice.Api.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class BaseController : ControllerBase
    {
        protected readonly IRequestValidator RequestValidator;

        public BaseController(IRequestValidator requestValidator)
        {
            RequestValidator = requestValidator;
        }
        
        protected int GetUserId()
        {
            var userIdentifier = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            
            return userIdentifier == null 
                ? 0 
                : Convert.ToInt32(userIdentifier);
        }
    }
}