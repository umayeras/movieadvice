using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Model.Requests;
using MovieAdvice.Validation.Abstract;

namespace MovieAdvice.Api.Controllers
{
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService accountService;
        private readonly IRequestValidator requestValidator;

        public AccountController(IAccountService accountService, IRequestValidator requestValidator)
        {
            this.accountService = accountService;
            this.requestValidator = requestValidator;
        }

        [HttpPost, Route("/account/login")]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Login([FromBody] LoginUserRequest request)
        {
            var validationResult = requestValidator.Validate(request);
            if (!validationResult.IsValid)
            {
                return BadRequest(Messages.InvalidRequest);
            }

            var result = await accountService.Authenticate(request);
            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }

            return Ok(result.Data);
        }
        
        [HttpPost, Route("/account/register")]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Register([FromBody] RegisterUserRequest request)
        {
            var validationResult = requestValidator.Validate(request);
            if (!validationResult.IsValid)
            {
                return BadRequest(Messages.InvalidRequest);
            }

            var result = await accountService.RegisterUser(request);
            if (!result.IsSuccess)
            {
                return BadRequest(result.Message);
            }

            return Ok(result.Data);
        }
    }
}