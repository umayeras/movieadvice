using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Model.Requests;
using MovieAdvice.Validation.Abstract;

namespace MovieAdvice.Api.Controllers
{
    public class MovieCommentsController : BaseController
    {
        #region ctor

        private readonly IMovieService movieService;
        private readonly IMovieCommentService commentService;

        public MovieCommentsController(
            IRequestValidator requestValidator,
            IMovieService movieService,
            IMovieCommentService commentService) : base(requestValidator)
        {
            this.movieService = movieService;
            this.commentService = commentService;
        }

        #endregion

        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Post(AddCommentRequest request)
        {
            var validationResult = RequestValidator.Validate(request);
            if (!validationResult.IsValid)
            {
                return BadRequest(Messages.InvalidRequest);
            }

            var movie = await movieService.GetById(request.MovieId);
            if (movie == null)
            {
                return NotFound(Messages.MovieNotFound);
            }

            var userId = GetUserId();
            if (userId == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            var result = await commentService.Add(request, userId);
            if (!result.IsSuccess)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok(result.Message);
        }
    }
}