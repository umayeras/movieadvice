using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Model.Requests;
using MovieAdvice.Validation.Abstract;

namespace MovieAdvice.Api.Controllers
{
    public class MovieScoresController : BaseController
    {
        private readonly IMovieScoreService scoreService;
        private readonly IMovieService movieService;

        public MovieScoresController(
            IRequestValidator requestValidator,
            IMovieScoreService scoreService,
            IMovieService movieService) : base(requestValidator)
        {
            this.scoreService = scoreService;
            this.movieService = movieService;
        }

        [HttpPost]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Post([FromBody] AddScoreRequest request)
        {
            var validationResult = RequestValidator.Validate(request);
            if (!validationResult.IsValid)
            {
                return BadRequest(Messages.InvalidRequest);
            }

            var movie = await movieService.GetById(request.MovieId);
            if (movie == null)
            {
                return NotFound(Messages.MovieNotFound);
            }

            var userId = GetUserId();
            if (userId == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            var result = await scoreService.Add(request, userId);
            
            return Ok(result.Message);
        }
    }
}