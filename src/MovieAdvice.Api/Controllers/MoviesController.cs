﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Core.Helpers.Pagination;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;
using MovieAdvice.Validation.Abstract;

namespace MovieAdvice.Api.Controllers
{
    public class MoviesController : BaseController
    {
        private readonly IMovieService movieService;
        private readonly IMovieRecommendService recommendService;
        
        public MoviesController(
            IRequestValidator requestValidator,
            IMovieService movieService, IMovieRecommendService recommendService) : base(requestValidator)
        {
            this.movieService = movieService;
            this.recommendService = recommendService;
        }

        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Movie>), (int) HttpStatusCode.OK)]
        public async Task<PagedResponse<List<Movie>>> Get([FromQuery] PaginationFilter filter)
        {
            if (filter.PageNumber <= 0 || filter.PageSize <= 0)
            {
                filter.PageNumber = 1;
                filter.PageSize = 10;
            }

            return await movieService.GetAllByPagination("/movies", filter);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Movie), (int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Get(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var userId = GetUserId();
            if (userId == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
            var result = await movieService.GetByDetail(id, userId);
            if (result.Movie == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
        
        [HttpPost,Route("/movies/recommend")]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.BadRequest)]
        [ProducesResponseType((int) HttpStatusCode.NotFound)]
        [ProducesResponseType((int) HttpStatusCode.InternalServerError)]
        public async Task<IActionResult> Recommend(RecommendMovieRequest request)
        {
            var validationResult = RequestValidator.Validate(request);
            if (!validationResult.IsValid)
            {
                return BadRequest(Messages.InvalidRequest);
            }

            var movie = await movieService.GetById(request.MovieId);
            if (movie == null)
            {
                return NotFound(Messages.MovieNotFound);
            }
            
            var userId = GetUserId();
            if (userId == 0)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
            var result = await recommendService.RecommendMovie(movie, userId, request.Email);
            if (!result.IsSuccess)
            {
               return StatusCode(StatusCodes.Status500InternalServerError);
            }

            return Ok(result.Message);
        }
    }
}