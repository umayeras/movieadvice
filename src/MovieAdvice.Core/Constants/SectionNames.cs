namespace MovieAdvice.Core.Constants
{
    public static class SectionNames
    {
        public static string MovieDbSettings => nameof(MovieDbSettings);
        public static string TokenOptions => nameof(TokenOptions);
        public static string EmailSettings => nameof(EmailSettings);
    }
}