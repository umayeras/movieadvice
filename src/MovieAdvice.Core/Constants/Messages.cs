namespace MovieAdvice.Core.Constants
{
    public static class Messages
    {
        public static string GeneralError => "Bir hata oluştu! Lütfen daha sonra tekrar deneyiniz!";
        public static string InvalidRequest => "Geçersiz istek";
        public static string UserNotFound => "Kullanıcı bulunamadı!";
        public static string UserRegisteredBefore => "Daha önce kayıt işlemi yapılmış!";
        public static string UserRegistrationError => "Kullanıcı kayıt edilirken bir hata oluştu!";
        public static string MovieIsRatedBefore => "Daha önce oy kullanılmış!!";
        public static string MovieScoreAdded => "Film oylandı.";
        public static string MovieScoreIsNotAdded => "Film oylama işlemi başarısız.";
        public static string MovieCommentAdded => "Film yorumu eklendi.";
        public static string MovieCommentIsNotAdded => "Film yorumu ekleme başarısız.";
        public static string MovieNotFound => "Film bulunamadı.";
        public static string DatabaseUpdated => "Veri tabanı güncellendi...";
        public static string TokenNotCreated => "Token oluşturulamadı!";
        public static string RecommendEmailSent => "Film tavsiyesi gönderildi.";
    }
}