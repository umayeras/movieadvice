﻿namespace MovieAdvice.Core.Constants
{
    public class MovieDbSettings
    {
        public string BaseAddress { get; set; }
        public string ApiKey { get; set; }
        public string GenresPath { get; set; }
        public string MoviesPath { get; set; }
    }
}
