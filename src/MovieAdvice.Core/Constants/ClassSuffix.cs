﻿﻿namespace MovieAdvice.Core.Constants
{
    public static class ClassSuffix
    {
        public static string Service => nameof(Service);
        public static string Factory => nameof(Factory);
    }
}
