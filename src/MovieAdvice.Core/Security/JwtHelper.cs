using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MovieAdvice.Core.Constants;
using MovieAdvice.Core.Security.Abstract;
using MovieAdvice.Model.Entities;

namespace MovieAdvice.Core.Security
{
    public class JwtHelper : ITokenHelper
    {
        private IConfiguration Configuration { get; }

        public JwtHelper(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public AccessToken CreateToken(User user)
        {
            var tokenOptions = Configuration.GetSection(SectionNames.TokenOptions).Get<TokenOptions>();
            var securityKey = SecurityHelper.CreateSecurityKey(tokenOptions.SecurityKey);
            var credentials = SecurityHelper.CreateSigningCredentials(securityKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Expires = DateTime.Now.AddMinutes(tokenOptions.TokenExpiration),
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
                }),
                SigningCredentials = credentials
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var createdToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(createdToken);

            return new AccessToken
            {
                Token = token,
                Expiration = DateTime.Now.AddMinutes(tokenOptions.TokenExpiration)
            };
        }
    }
}