using MovieAdvice.Model.Entities;

namespace MovieAdvice.Core.Security.Abstract
{
    public interface ITokenHelper
    {
        AccessToken CreateToken(User user);
    }
}