using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace MovieAdvice.Core.Security
{
    public static class PasswordHelper
    {
        public static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using var hmac = new HMACSHA512();

            passwordSalt = hmac.Key;
            passwordHash = ComputeHash(password, hmac);
        }

        public static bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using var hmac = new HMACSHA512(passwordSalt);

            var computedHash = ComputeHash(password, hmac);

            return !computedHash.Where((t, i) => t != passwordHash[i]).Any();
        }

        private static byte[] ComputeHash(string password, HMACSHA512 hmac)
        {
            return hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
        }
    }
}