namespace MovieAdvice.Core.Security
{
    public class TokenOptions
    {
        public int TokenExpiration { get; set; }
        public string SecurityKey { get; set; }
    }
}