using System;
using Microsoft.AspNetCore.WebUtilities;

namespace MovieAdvice.Core.Helpers.Pagination
{
    public interface IUriService
    {
        public Uri GetPageUri(PaginationFilter filter, string route);
    }
    
    public class UriService : IUriService
    {
        private readonly string baseUri;
        public UriService(string baseUri)
        {
            this.baseUri = baseUri;
        }
        
        public Uri GetPageUri(PaginationFilter filter ,string route)
        {
            var endpointUri = new Uri(string.Concat(baseUri ,route));
            var modifiedUri = QueryHelpers.AddQueryString(endpointUri.ToString(), "pageNumber", filter.PageNumber.ToString());
            modifiedUri = QueryHelpers.AddQueryString(modifiedUri, "pageSize", filter.PageSize.ToString());
            
            return new Uri(modifiedUri);
        }
    }
}