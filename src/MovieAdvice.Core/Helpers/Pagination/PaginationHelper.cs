using System;
using System.Collections.Generic;

namespace MovieAdvice.Core.Helpers.Pagination
{
    public static class PaginationHelper
    {
        public static PagedResponse<List<T>> CreatePagedResponse<T>(List<T> pagedData, PaginationFilter validFilter,
            int totalRecords, IUriService uriService, string route)
        {
            var response = new PagedResponse<List<T>>(pagedData, validFilter.PageNumber, validFilter.PageSize);
            var totalPages = (totalRecords / (double) validFilter.PageSize);
            var roundedTotalPages = Convert.ToInt32(Math.Ceiling(totalPages));

            response.NextPage = SetNextPage<T>(validFilter, uriService, route, roundedTotalPages);
            response.PreviousPage = SetPreviousPage<T>(validFilter, uriService, route, roundedTotalPages);
            response.FirstPage = SetFirstPage<T>(validFilter, uriService, route);
            response.LastPage = SetLastPage<T>(validFilter, uriService, route, roundedTotalPages);
            response.TotalPages = roundedTotalPages;
            response.TotalRecords = totalRecords;

            return response;
        }

        private static Uri SetLastPage<T>(PaginationFilter validFilter, IUriService uriService, string route,
            int roundedTotalPages)
        {
            return uriService.GetPageUri(new PaginationFilter(roundedTotalPages, validFilter.PageSize), route);
        }

        private static Uri SetFirstPage<T>(PaginationFilter validFilter, IUriService uriService, string route)
        {
            return uriService.GetPageUri(new PaginationFilter(1, validFilter.PageSize), route);
        }

        private static Uri SetPreviousPage<T>(PaginationFilter validFilter, IUriService uriService, string route,
            int roundedTotalPages)
        {
            return validFilter.PageNumber - 1 >= 1 && validFilter.PageNumber <= roundedTotalPages
                ? uriService.GetPageUri(new PaginationFilter(validFilter.PageNumber - 1, validFilter.PageSize), route)
                : null;
        }

        private static Uri SetNextPage<T>(PaginationFilter validFilter, IUriService uriService, string route,
            int roundedTotalPages)
        {
            return validFilter.PageNumber >= 1 && validFilter.PageNumber < roundedTotalPages
                ? uriService.GetPageUri(new PaginationFilter(validFilter.PageNumber + 1, validFilter.PageSize), route)
                : null;
        }
    }
}