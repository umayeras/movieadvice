using System.Threading.Tasks;

namespace MovieAdvice.Core.Mailing.Abstract
{
    public interface IEmailService
    {
        Task<SendEmailResult> SendEmailAsync(EmailRequest request);
    }
}