using FluentValidation.TestHelper;
using MovieAdvice.Validation.RequestValidators;
using NUnit.Framework;

namespace MovieAdvice.Tests.Validation
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class LoginUserRequestValidatorTests
    {
        #region members & setup

        private LoginUserRequestValidator validator;

        [SetUp]
        public void Init()
        {
            validator = new LoginUserRequestValidator();
        }

        #endregion

        #region invalid conditions

        [Test]
        public void Validate_ValueEmpty_HaveError()
        {
            validator.ShouldHaveValidationErrorFor(model => model.Email, string.Empty);
            validator.ShouldHaveValidationErrorFor(model => model.Password, string.Empty);
        }

        #endregion

        #region valid conditions

        [Test]
        public void Validate_ValueNotEmpty_DoNotHaveError()
        {
            validator.ShouldNotHaveValidationErrorFor(model => model.Email, "email@email.com");
            validator.ShouldNotHaveValidationErrorFor(model => model.Password, "12345");
        }

        #endregion
    }
}