using FluentValidation.TestHelper;
using MovieAdvice.Validation.RequestValidators;
using NUnit.Framework;

namespace MovieAdvice.Tests.Validation
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class AddScoreRequestValidatorTests
    {
        #region members & setup

        private AddScoreRequestValidator validator;

        [SetUp]
        public void Init()
        {
            validator = new AddScoreRequestValidator();
        }

        #endregion

        #region invalid conditions

        [Test]
        public void Validate_ValueEmpty_HaveError()
        {
            validator.ShouldHaveValidationErrorFor(model => model.MovieId, 0);
            validator.ShouldHaveValidationErrorFor(model => model.MovieId, -1);
            validator.ShouldHaveValidationErrorFor(model => model.Score, 0);
            validator.ShouldHaveValidationErrorFor(model => model.Score, 11);
        }

        #endregion

        #region valid conditions

        [Test]
        public void Validate_ValueNotEmpty_DoNotHaveError()
        {
            validator.ShouldNotHaveValidationErrorFor(model => model.MovieId, 1);
            validator.ShouldNotHaveValidationErrorFor(model => model.Score, 5);
            validator.ShouldNotHaveValidationErrorFor(model => model.Score, 9);
        }

        #endregion
    }
}