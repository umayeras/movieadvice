using FluentValidation.TestHelper;
using MovieAdvice.Validation.RequestValidators;
using NUnit.Framework;

namespace MovieAdvice.Tests.Validation
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class AddCommentRequestValidatorTests
    {
        #region members & setup

        private AddCommentRequestValidator validator;

        [SetUp]
        public void Init()
        {
            validator = new AddCommentRequestValidator();
        }

        #endregion

        #region invalid conditions

        [Test]
        public void Validate_ValueEmpty_HaveError()
        {
            validator.ShouldHaveValidationErrorFor(model => model.MovieId, 0);
            validator.ShouldHaveValidationErrorFor(model => model.MovieId, -1);
            validator.ShouldHaveValidationErrorFor(model => model.Comment, string.Empty);
        }

        #endregion

        #region valid conditions

        [Test]
        public void Validate_ValueNotEmpty_DoNotHaveError()
        {
            validator.ShouldNotHaveValidationErrorFor(model => model.MovieId, 1);
            validator.ShouldNotHaveValidationErrorFor(model => model.Comment, "some comments");
        }

        #endregion
    }
}