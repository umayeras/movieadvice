using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using MovieAdvice.Business.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Core.Security;
using MovieAdvice.Core.Security.Abstract;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;
using MovieAdvice.Tests.Helpers;
using NUnit.Framework;

namespace MovieAdvice.Tests.Business.Services
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class AccountServiceTests
    {
        #region members & setup
    
        private AccountService service;
        private Mock<ILogger<AccountService>> logger;
        private Mock<IUnitOfWork> unitOfWork;
        private Mock<IBaseRepository<User>> repository;
        private Mock<ITokenHelper> tokenHelper;
        
        [SetUp]
        public void Init()
        {
            logger = new Mock<ILogger<AccountService>>();
            unitOfWork = new Mock<IUnitOfWork>();
            repository = new Mock<IBaseRepository<User>>();
            tokenHelper = new Mock<ITokenHelper>();
            unitOfWork.Setup(x => x.GetRepository<User>()).Returns(repository.Object);
    
            service = new AccountService(
                unitOfWork.Object,
                logger.Object,
                tokenHelper.Object);
        }
    
        #endregion
    
        [Test]
        public async Task Authenticate_UserNotFound_ReturnErrorResult()
        {
            // Arrange
            var request = new LoginUserRequest{ Email = "email", Password = "123"};
            var serviceResult = ServiceDataResult.Error(Messages.UserNotFound);
            
            repository.Setup(x => x.GetAsync(m => m.Email == request.Email)).ReturnsAsync((User) null);

            // Act
            var result = await service.Authenticate(request);
    
            // Assert
            result.Should().BeEquivalentTo(serviceResult);
            AssertHelpers.VerifyLogger(logger, LogLevel.Error, Times.Once());
        }
        
        [Test]
        public async Task Authenticate_TokenNotCreated_ReturnErrorResult()
        {
            // Arrange
            var request = new LoginUserRequest{ Email = "email", Password = "123"};
            var user = new User();
            var serviceResult = ServiceDataResult.Error(Messages.TokenNotCreated);
            
            repository.Setup(x => x.GetAsync(m => m.Email == request.Email)).ReturnsAsync(user);
            tokenHelper.Setup(x => x.CreateToken(user)).Returns((AccessToken) null);
            
            // Act
            var result = await service.Authenticate(request);
    
            // Assert
            result.Should().BeEquivalentTo(serviceResult);
            AssertHelpers.VerifyLogger(logger, LogLevel.Error, Times.Once());
        }
        
        [Test]
        public async Task Authenticate_TokenCreatedSuccess_ReturnAccessToken()
        {
            // Arrange
            var request = new LoginUserRequest{ Email = "email", Password = "123"};
            var user = new User();
            var token = new AccessToken();
            var serviceResult = ServiceDataResult.Success(token);
            
            repository.Setup(x => x.GetAsync(m => m.Email == request.Email)).ReturnsAsync(user);
            tokenHelper.Setup(x => x.CreateToken(user)).Returns(token);
            
            // Act
            var result = await service.Authenticate(request);
    
            // Assert
            result.Should().BeEquivalentTo(serviceResult);
        }
    }
}