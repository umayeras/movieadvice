using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using MovieAdvice.Business.Abstract.Factories;
using MovieAdvice.Business.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;
using NUnit.Framework;

namespace MovieAdvice.Tests.Business.Services
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class MovieCommentServiceTests
    {
        #region members & setup

        private MovieCommentService service;
        private Mock<ILogger<MovieCommentService>> logger;
        private Mock<IUnitOfWork> unitOfWork;
        private Mock<IBaseRepository<MovieComment>> repository;
        private Mock<IMovieCommentFactory> factory;

        [SetUp]
        public void Init()
        {
            logger = new Mock<ILogger<MovieCommentService>>();
            unitOfWork = new Mock<IUnitOfWork>();
            repository = new Mock<IBaseRepository<MovieComment>>();
            factory = new Mock<IMovieCommentFactory>();
            unitOfWork.Setup(x => x.GetRepository<MovieComment>()).Returns(repository.Object);

            service = new MovieCommentService(
                unitOfWork.Object,
                logger.Object,
                factory.Object);
        }

        #endregion

        [Test]
        public async Task GetUserComments_NoCondition_ReturnUserComments()
        {
            // Arrange
            const int userId = 1;
            const int movieId = 1;
            var list = new List<MovieComment>().AsQueryable();

            repository.Setup(x => x.GetAllAsync(m => m.MovieId == movieId && m.CreatedBy == userId)).ReturnsAsync(list);

            // Act
            var result = await service.GetUserComments(userId, movieId);

            // Assert
            result.Should().BeEquivalentTo(list);
        }

        [Test]
        public async Task Add_AddingFails_ReturnServiceErrorResult()
        {
            // Arrange
            const int userId = 1;
            var request = new AddCommentRequest();
            var model = new MovieComment();
            var exception = It.IsAny<Exception>();
            var serviceResult = ServiceResult.Error(Messages.MovieCommentIsNotAdded);

            factory.Setup(x => x.CreateMovieComment(request, userId)).Returns(model);
            repository.Setup(x => x.AddAsync(model)).ThrowsAsync(exception);

            // Act
            var result = await service.Add(request, userId);

            // Assert
            result.Should().BeEquivalentTo(serviceResult);
            unitOfWork.Verify(x => x.Save(), Times.Never);
        }

        [Test]
        public async Task Add_AddingSuccess_ReturnServiceSuccessResult()
        {
            // Arrange
            const int userId = 1;
            var request = new AddCommentRequest();
            var model = new MovieComment();
            const bool isAdded = true;
            var serviceResult = ServiceResult.Success(Messages.MovieCommentAdded);

            factory.Setup(x => x.CreateMovieComment(request, userId)).Returns(model);
            repository.Setup(x => x.AddAsync(model)).ReturnsAsync(isAdded);

            // Act
            var result = await service.Add(request, userId);

            // Assert
            result.Should().BeEquivalentTo(serviceResult);
            unitOfWork.Verify(x => x.Save(), Times.Once);
        }
    }
}