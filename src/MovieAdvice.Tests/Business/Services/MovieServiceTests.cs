using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Business.Services;
using MovieAdvice.Core.Helpers.Pagination;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Model.Entities;
using NUnit.Framework;

namespace MovieAdvice.Tests.Business.Services
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class MovieServiceTests
    {
        #region members & setup
    
        private MovieService service;
        private Mock<ILogger<MovieService>> logger;
        private Mock<IUnitOfWork> unitOfWork;
        private Mock<IBaseRepository<Movie>> repository;
        private Mock<IUriService> uriService;
        private Mock<IMovieScoreService> movieScoreService;
        private Mock<IMovieCommentService> movieCommentService;

        [SetUp]
        public void Init()
        {
            logger = new Mock<ILogger<MovieService>>();
            unitOfWork = new Mock<IUnitOfWork>();
            repository = new Mock<IBaseRepository<Movie>>();
            uriService = new Mock<IUriService>();
            movieCommentService = new Mock<IMovieCommentService>();
            movieScoreService = new Mock<IMovieScoreService>();
            unitOfWork.Setup(x => x.GetRepository<Movie>()).Returns(repository.Object);
    
            service = new MovieService(
                unitOfWork.Object,
                logger.Object,
                uriService.Object,
                movieCommentService.Object,
                movieScoreService.Object);
        }
    
        #endregion
    
        [Test]
        public async Task GetById_NoCondition_ReturnMovie()
        {
            // Arrange
            var id = It.IsAny<int>();
            var movie = new Movie();
    
            repository.Setup(x => x.GetAsync(m => m.Id == id)).ReturnsAsync(movie);
    
            // Act
            var result = await service.GetById(id);
    
            // Assert
            result.Should().BeEquivalentTo(movie);
        }
    }
}