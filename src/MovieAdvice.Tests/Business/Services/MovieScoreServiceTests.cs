using System;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using MovieAdvice.Business.Abstract.Factories;
using MovieAdvice.Business.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Data.Repositories;
using MovieAdvice.Data.Uow;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;
using NUnit.Framework;

namespace MovieAdvice.Tests.Business.Services
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class MovieScoreServiceTests
    {
        #region members & setup

        private MovieScoreService service;
        private Mock<ILogger<MovieScoreService>> logger;
        private Mock<IUnitOfWork> unitOfWork;
        private Mock<IBaseRepository<MovieScore>> repository;
        private Mock<IMovieScoreFactory> factory;

        [SetUp]
        public void Init()
        {
            logger = new Mock<ILogger<MovieScoreService>>();
            unitOfWork = new Mock<IUnitOfWork>();
            repository = new Mock<IBaseRepository<MovieScore>>();
            factory = new Mock<IMovieScoreFactory>();
            unitOfWork.Setup(x => x.GetRepository<MovieScore>()).Returns(repository.Object);

            service = new MovieScoreService(
                unitOfWork.Object,
                logger.Object,
                factory.Object);
        }

        #endregion

        [Test]
        public async Task GetUserScore_NoCondition_ReturnUserScore()
        {
            // Arrange
            const int userId = 1;
            const int movieId = 1;

            repository.Setup(x => x.GetAsync(s => s.MovieId == movieId && s.CreatedBy == userId))
                .ReturnsAsync((MovieScore) null);

            // Act
            var result = await service.GetUserScore(userId, movieId);

            // Assert
            result.Should().Be(0);
        }

        [Test]
        public async Task Add_AddingFails_ReturnServiceErrorResult()
        {
            // Arrange
            const int userId = 1;
            var request = new AddScoreRequest();
            var model = new MovieScore();
            var exception = It.IsAny<Exception>();
            var serviceResult = ServiceResult.Error(Messages.MovieScoreIsNotAdded);

            factory.Setup(x => x.CreateMovieScore(request, userId)).Returns(model);
            repository.Setup(x => x.AddAsync(model)).ThrowsAsync(exception);

            // Act
            var result = await service.Add(request, userId);

            // Assert
            result.Should().BeEquivalentTo(serviceResult);
            unitOfWork.Verify(x => x.Save(), Times.Never);
        }

        [Test]
        public async Task Add_AddingSuccess_ReturnServiceSuccessResult()
        {
            // Arrange
            const int userId = 1;
            var request = new AddScoreRequest();
            var model = new MovieScore();
            const bool isAdded = true;
            var serviceResult = ServiceResult.Success(Messages.MovieScoreAdded);

            factory.Setup(x => x.CreateMovieScore(request, userId)).Returns(model);
            repository.Setup(x => x.AddAsync(model)).ReturnsAsync(isAdded);

            // Act
            var result = await service.Add(request, userId);

            // Assert
            result.Should().BeEquivalentTo(serviceResult);
            unitOfWork.Verify(x => x.Save(), Times.Once);
        }
    }
}