using FluentAssertions;
using MovieAdvice.Business.Factories;
using MovieAdvice.Model.Requests;
using NUnit.Framework;

namespace MovieAdvice.Tests.Business.Factories
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class MovieScoreFactoryTests
    {
        #region members & setup
    
        private MovieScoreFactory factory;

        [SetUp]
        public void Init()
        {
            factory = new MovieScoreFactory();
        }
    
        #endregion
    
        [Test]
        public void CreateMovieComment_NoCondition_ReturnModel()
        {
            // Arrange
            var request = new AddScoreRequest
            {
                Score = 1,
                MovieId = 1
            };
            const int userId = 1;
            
            // Act
            var result = factory.CreateMovieScore(request, userId);

            // Assert
            result.Score.Should().Be(request.Score);
            result.MovieId.Should().Be(request.MovieId);
            result.CreatedBy.Should().Be(userId);
        }
    }
}