using FluentAssertions;
using MovieAdvice.Business.Factories;
using MovieAdvice.Model.Requests;
using NUnit.Framework;

namespace MovieAdvice.Tests.Business.Factories
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class MovieCommentFactoryTests
    {
        #region members & setup
    
        private MovieCommentFactory factory;

        [SetUp]
        public void Init()
        {
            factory = new MovieCommentFactory();
        }
    
        #endregion
    
        [Test]
        public void CreateMovieComment_NoCondition_ReturnModel()
        {
            // Arrange
            var request = new AddCommentRequest
            {
                Comment = "comment",
                MovieId = 1
            };
            const int userId = 1;
            
            // Act
            var result = factory.CreateMovieComment(request, userId);

            // Assert
            result.Comment.Should().Be(request.Comment);
            result.MovieId.Should().Be(request.MovieId);
            result.CreatedBy.Should().Be(userId);
        }
    }
}