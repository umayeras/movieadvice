using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using MovieAdvice.Api.Controllers;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;
using MovieAdvice.Validation;
using MovieAdvice.Validation.Abstract;
using NUnit.Framework;

namespace MovieAdvice.Tests.Controller
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class AccountControllerTests
    {
        #region members & setup

        private AccountController controller;
        private Mock<IRequestValidator> requestValidator;
        private Mock<IAccountService> accountService;

        [SetUp]
        public void Init()
        {
            requestValidator = new Mock<IRequestValidator>();
            accountService = new Mock<IAccountService>();

            controller = new AccountController(accountService.Object, requestValidator.Object);
        }

        #endregion

        [Test]
        public async Task Post_InvalidRequest_ReturnBadRequest()
        {
            // Arrange
            var request = new LoginUserRequest();
            var validationResult = ValidationResult.Error("invalid request");
            var apiResult = new BadRequestObjectResult(Messages.InvalidRequest);

            requestValidator.Setup(x => x.Validate(request)).Returns(validationResult);

            // Act
            var result = await controller.Login(request);

            // Assert
            result.Should().BeEquivalentTo(apiResult);
        }

        [Test]
        public async Task Post_AuthenticateUser_ReturnOkObjectResult()
        {
            // Arrange
            var request = new LoginUserRequest();
            var validationResult = ValidationResult.Success;
            var apiResult = new OkObjectResult(It.IsAny<object>());
            var serviceResult = ServiceDataResult.Success(It.IsAny<object>());

            requestValidator.Setup(x => x.Validate(request)).Returns(validationResult);
            accountService.Setup(x => x.Authenticate(request)).ReturnsAsync(serviceResult);
            
            // Act
            var result = await controller.Login(request);

            // Assert
            result.Should().BeEquivalentTo(apiResult);
        }
    }
}