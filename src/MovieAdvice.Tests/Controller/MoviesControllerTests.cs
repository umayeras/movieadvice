using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using MovieAdvice.Api.Controllers;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Helpers.Pagination;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Results;
using MovieAdvice.Validation.Abstract;
using NUnit.Framework;

namespace MovieAdvice.Tests.Controller
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class MoviesControllerTests
    {
        #region members & setup

        private MoviesController controller;
        private Mock<IMovieService> movieService;
        private Mock<IRequestValidator> requestValidator;
        private Mock<IMovieRecommendService> recommendService;

        [SetUp]
        public void Init()
        {
            movieService = new Mock<IMovieService>();
            requestValidator = new Mock<IRequestValidator>();
            recommendService = new Mock<IMovieRecommendService>();

            controller = new MoviesController(requestValidator.Object, movieService.Object, recommendService.Object);
        }

        #endregion

        [Test]
        public async Task Get_PageNumberAndSizeIsNull_ReturnList()
        {
            // Arrange
            var filter = new PaginationFilter {PageNumber = 0, PageSize = 0};
            var route = "/movies";
            var movies = new List<Movie>();
            var validFilter = new PaginationFilter {PageNumber = 1, PageSize = 10};

            var serviceResult = new PagedResponse<List<Movie>>(movies, validFilter.PageNumber, validFilter.PageSize);

            movieService.Setup(x => x.GetAllByPagination(route, filter)).ReturnsAsync(serviceResult);

            // Act
            var result = await controller.Get(filter);

            // Assert
            result.Should().BeOfType<PagedResponse<List<Movie>>>();
        }

        [Test]
        public async Task Get_NoCondition_ReturnList()
        {
            // Arrange
            var filter = new PaginationFilter {PageNumber = 1, PageSize = 5};
            var route = "/movies";
            var movies = new List<Movie>();
            var serviceResult = new PagedResponse<List<Movie>>(movies, filter.PageNumber, filter.PageSize);

            movieService.Setup(x => x.GetAllByPagination(route, filter)).ReturnsAsync(serviceResult);

            // Act
            var result = await controller.Get(filter);

            // Assert
            result.Should().BeOfType<PagedResponse<List<Movie>>>();
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public async Task Get_IdValueIsZeroOrLessThanZero_ReturnBadRequest(int id)
        {
            // No Arrange

            // Act
            var result = await controller.Get(id);

            // Assert
            result.Should().BeOfType<BadRequestResult>();
            movieService.Verify(x => x.GetById(id), Times.Never);
        }

        [Test]
        public async Task Get_UserIdError_ReturnServerError()
        {
            // Arrange
            const int id = 1;
            var apiResult = new StatusCodeResult(StatusCodes.Status500InternalServerError);
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, "1"),
            }, "mock"));

            controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext() {User = null}
            };

            // Act
            var result = await controller.Get(id);

            // Assert
            result.Should().BeEquivalentTo(apiResult);
        }


        [Test]
        public async Task Get_MovieNotFound_ReturnNotFound()
        {
            // Arrange
            const int id = 1;
            var serviceResult = new MovieDetailResult {Movie = null};
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, "1"),
            }, "mock"));

            controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext() {User = user}
            };

            var userId = Convert.ToInt32(user.FindFirstValue(ClaimTypes.NameIdentifier));

            movieService.Setup(x => x.GetByDetail(id, userId)).ReturnsAsync(serviceResult);

            // Act
            var result = await controller.Get(id);

            // Assert
            result.Should().BeOfType<NotFoundResult>();
        }

        [Test]
        public async Task Get_NoCondition_ReturnMovie()
        {
            // Arrange
            const int id = 1;
            var serviceResult = new MovieDetailResult
            {
                Movie = new Movie(),
                Comments = new List<MovieComment>(),
                AverageScore = 1,
                UserScore = 1
            };

            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, "1"),
            }, "mock"));

            controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext() {User = user}
            };

            var userId = Convert.ToInt32(user.FindFirstValue(ClaimTypes.NameIdentifier));

            movieService.Setup(x => x.GetByDetail(id, userId)).ReturnsAsync(serviceResult);

            // Act
            var result = await controller.Get(id);

            // Assert
            result.Should().BeOfType<OkObjectResult>().Which.Value.As<MovieDetailResult>();
        }
    }
}