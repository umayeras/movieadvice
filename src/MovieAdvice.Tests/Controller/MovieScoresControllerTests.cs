using System;
using System.Security.Claims;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using MovieAdvice.Api.Controllers;
using MovieAdvice.Business.Abstract.Services;
using MovieAdvice.Core.Constants;
using MovieAdvice.Model.Entities;
using MovieAdvice.Model.Requests;
using MovieAdvice.Model.Results;
using MovieAdvice.Validation;
using MovieAdvice.Validation.Abstract;
using NUnit.Framework;

namespace MovieAdvice.Tests.Controller
{
    [TestFixture]
    [Parallelizable(ParallelScope.Fixtures)]
    public class MovieScoresControllerTests
    {
        #region members & setup

        private MovieScoresController controller;
        private Mock<IRequestValidator> requestValidator;
        private Mock<IMovieService> movieService;
        private Mock<IMovieScoreService> movieScoreService;

        [SetUp]
        public void Init()
        {
            requestValidator = new Mock<IRequestValidator>();
            movieService = new Mock<IMovieService>();
            movieScoreService = new Mock<IMovieScoreService>();
            controller = new MovieScoresController(
                requestValidator.Object,
                movieScoreService.Object,
                movieService.Object);
        }

        #endregion

        [Test]
        public async Task Post_InvalidRequest_ReturnBadRequest()
        {
            // Arrange
            var request = new AddScoreRequest();
            var validationResult = ValidationResult.Error("invalid request");
            var apiResult = new BadRequestObjectResult(Messages.InvalidRequest);

            requestValidator.Setup(x => x.Validate(request)).Returns(validationResult);

            // Act
            var result = await controller.Post(request);

            // Assert
            result.Should().BeEquivalentTo(apiResult);
        }
        
        [Test]
        public async Task Post_MovieNotFound_ReturnNotFound()
        {
            // Arrange
            var request = new AddScoreRequest { MovieId = 200, Score = 10};
            var validationResult = ValidationResult.Success;
            var apiResult = new NotFoundObjectResult(Messages.MovieNotFound);

            requestValidator.Setup(x => x.Validate(request)).Returns(validationResult);
            movieService.Setup(x => x.GetById(request.MovieId)).ReturnsAsync((Movie) null);

            // Act
            var result = await controller.Post(request);

            // Assert
            result.Should().BeEquivalentTo(apiResult);
        }
        
        [Test]
        public async Task Post_UserIdError_ReturnServerError()
        {
            // Arrange
            var request = new AddScoreRequest { MovieId = 200, Score = 10};
            var validationResult = ValidationResult.Success;
            var apiResult = new StatusCodeResult(StatusCodes.Status500InternalServerError);
            var movie = new Movie{ Id = 200};

            requestValidator.Setup(x => x.Validate(request)).Returns(validationResult);
            movieService.Setup(x => x.GetById(request.MovieId)).ReturnsAsync(movie);
            
            controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext() { User = null }
            };
            
            // Act
            var result = await controller.Post(request);

            // Assert
            result.Should().BeEquivalentTo(apiResult);
        }
       
        [Test]
        public async Task Post_NoCondition_ReturnOkResult()
        {
            // Arrange
            var request = new AddScoreRequest { MovieId = 200, Score = 10};
            var validationResult = ValidationResult.Success;
            var movie = new Movie{ Id = 200};
            var serviceResult = ServiceResult.Success(Messages.MovieScoreAdded);
            var apiResult = new OkObjectResult(serviceResult.Message);

            requestValidator.Setup(x => x.Validate(request)).Returns(validationResult);
            movieService.Setup(x => x.GetById(request.MovieId)).ReturnsAsync(movie);
            
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, "1"),
            }, "mock"));
            
            controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext() { User = user }
            };

            var userId = Convert.ToInt32(user.FindFirstValue(ClaimTypes.NameIdentifier));

            movieScoreService.Setup(x => x.Add(request, userId)).ReturnsAsync(serviceResult);
            
            // Act
            var result = await controller.Post(request);

            // Assert
            result.Should().BeEquivalentTo(apiResult);
        }
    }
}