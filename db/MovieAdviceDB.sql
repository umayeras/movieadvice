CREATE TABLE Users
(
  Id int identity(1,1) primary key not null,
  Email nvarchar(200) not null,
  Password nvarchar(200) not null
)
GO

CREATE TABLE Genres
(
  Id int primary key not null,
  Name nvarchar(100) not null
)
GO

CREATE TABLE Movies
(
  Id int identity(1,1) primary key not null,
  Title nvarchar(200) not null,
  Poster nvarchar(200) null,
  Overview nvarchar(max)
)
GO

CREATE TABLE MovieComments
(
  Id int identity(1,1) primary key not null,
  MovieId int not null,
  Comment nvarchar(max) not null,
  CreatedBy int references Users(Id),
  CreatedDate datetime default getdate(),
)
GO

CREATE TABLE MovieScores
(
  Id int identity(1,1) primary key not null,
  MovieId int not null,
  Score int not null,
  CreatedBy int references Users(Id),
  CreatedDate datetime default getdate(),
)
GO